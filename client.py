import discord

import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from model.bill import Bill
from model.state import State
from fetch import Fetcher

KEYPHRASE = "repair"
USE_CACHE = True

engine = create_engine("postgresql+psycopg2://postgres:postgres@localhost/right_to_repair", echo=False)
Session = sessionmaker(bind=engine)
session = Session()

with open("api_key.txt") as api_key_text_file:
    api_key = api_key_text_file.read()

fetcher = Fetcher(api_key, session, KEYPHRASE, use_cache=USE_CACHE)

class RightToRepairClient(discord.Client):
    async def on_ready(self):
        self.state_new_updated_bills_dict = fetcher.fetch_all()
    
    async def on_message(self, message):
        if message.content.startswith("$latest"):
            await self.show(message.channel)
    
    async def show(self, channel):
        new_bills_state_strs = []
        updated_bills_state_strs = []
        if len(self.state_new_updated_bills_dict) == 0:
            message = "No updates."
            await channel.send(message)
            return
        else:
            for state in self.state_new_updated_bills_dict:
                new_bills, updated_bills = self.state_new_updated_bills_dict[state]
                
                if len(new_bills) > 0:
                    new_bill_strs = ["**{}**\n`Title:` {}\n`URL:` {}".format(
                        bill.number, bill.title, bill.url
                    ) for bill in new_bills]
                    new_bills_state_str = "{} ({}):\n{}\n".format(state.name, state.abbreviation, "\n\n".join(new_bill_strs))
                    new_bills_state_strs.append(new_bills_state_str)
                
                if len(updated_bills) > 0:
                    updated_bill_strs = ["**{}**\n`Title:` {}\n`Status:` {}\n{}\n`Status Date:` {}\n`Last Action:` {}\n`Last Action Date:` {}\n`URL:` {}\n".format(
                        bill.number, bill.url, bill.status, bill.status_date, bill.last_action, bill.last_action_date, bill.title, bill.description
                    ) for bill in updated_bills]
                    updated_bills_state_str = "{} ({}):\n{}\n".format(state.name, state.abbreviation, "\n\n".join(updated_bill_strs))
                    updated_bills_state_strs.append(updated_bills_state_str)
            
            if len(new_bills_state_strs) > 0 and len(updated_bills_state_strs) > 0:
                message = "**New bills:**\n\n{}\n\n**Updated bills:**\n\n{}".format("\n\n".join(new_bills_state_strs), "\n\n".join(updated_bills_state_strs))
            elif len(new_bills_state_strs) > 0:
                message = "**New bills:**\n\n{}".format("\n\n".join(new_bills_state_strs))
            else:
                message = "**Updated bills:**\n\n{}".format("\n\n".join(updated_bills_state_strs))
        
        offset = 0
        while offset < len(message):
            chunk = message[offset:offset+2000]
            reversed_chunk = chunk[::-1]
            length = reversed_chunk.find("\n")
            chunk = chunk[:2000 - length]
            offset += 2000 - length
            await channel.send(chunk)
    
with open("token.txt") as token_text_file:
    token = token_text_file.read()
client = RightToRepairClient()
client.run(token)
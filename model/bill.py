from datetime import datetime

from sqlalchemy import Column, Integer, String, Binary, Date, ForeignKey
from sqlalchemy.orm import relationship

from model.base import Base

class Bill(Base):
    __tablename__ = "bill"

    bill_id = Column(Integer, primary_key=True, nullable=False)
    state_id = Column(Integer, ForeignKey("state.id"), nullable=False)
    change_hash = Column(Binary(128), nullable=False)
    number = Column(String, nullable=False)
    url = Column(String, nullable=False)
    status_date = Column(Date, nullable=True)
    status = Column(Integer, nullable=False)
    last_action_date = Column(Date, nullable=True)
    last_action = Column(String, nullable=True)
    title = Column(String, nullable=False)
    description = Column(String, nullable=False)

    state = relationship("State", back_populates="bills")

    STATUS_DATE_FORMAT = "%Y-%m-%d"

    def update(self, bill_json_obj):
        change_hash = bytes.fromhex(bill_json_obj["change_hash"])
        if change_hash != self.change_hash:
            self.change_hash = change_hash
            self.number = bill_json_obj["number"]
            self.url = bill_json_obj["url"]
            status_date_str = bill_json_obj["status_date"]
            if status_date_str is None:
                self.status_date = None
            else:
                self.status_date = datetime.strptime(status_date_str, __class__.STATUS_DATE_FORMAT).date()
            self.status = int(bill_json_obj["status"])
            last_action_date_str = bill_json_obj["last_action_date"]
            if last_action_date_str is None:
                self.last_action_date = None
            else:
                self.last_action_date = datetime.strptime(last_action_date_str, __class__.STATUS_DATE_FORMAT).date()
            self.last_action = bill_json_obj["last_action"]
            self.title = bill_json_obj["title"]
            self.description = bill_json_obj["description"]
            return True
        return False
    
    @staticmethod
    def from_json_obj(state, bill_json_obj):
        bill_id = bill_json_obj["bill_id"]        
        change_hash = bytes.fromhex(bill_json_obj["change_hash"])
        number = bill_json_obj["number"]
        url = bill_json_obj["url"]
        status_date_str = bill_json_obj["status_date"]
        if status_date_str is None:
            status_date = None
        else:
            status_date = datetime.strptime(status_date_str, __class__.STATUS_DATE_FORMAT).date()
        status = int(bill_json_obj["status"])
        last_action_date_str = bill_json_obj["last_action_date"]
        if last_action_date_str is None:
            last_action_date = None
        else:
            last_action_date = datetime.strptime(last_action_date_str, __class__.STATUS_DATE_FORMAT).date()
        last_action = bill_json_obj["last_action"]
        title = bill_json_obj["title"]
        description = bill_json_obj["description"]
        return Bill(bill_id=bill_id, change_hash=change_hash,
            number=number, url=url, status_date=status_date, status=status,
            last_action_date=last_action_date, last_action=last_action,
            title=title, description=description, state=state)
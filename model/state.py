from sqlalchemy import Column, Integer, String, Binary, Date
from sqlalchemy.orm import relationship

from model.base import Base

class State(Base):
    __tablename__ = "state"

    id = Column(Integer, primary_key=True, nullable=False)
    abbreviation = Column(String, unique=True, nullable=False)
    name = Column(String, unique=True, nullable=False)

    bills = relationship("Bill")
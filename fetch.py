import os
import json
import requests

from pathlib import Path
from threading import Thread
from queue import Queue

from model.state import State
from update import get_new_updated_state_bills

class Fetcher:
    BASE_URL = "https://api.legiscan.com/"

    def __init__(self, api_key, session, keyphrase, use_cache=False):
        self.api_key = api_key
        self.session = session
        self.keyphrase = keyphrase
        self.use_cache = use_cache
        if (self.use_cache):
            try:
                os.mkdir("cache")
            except OSError:
                pass
    
    def fetch_all(self):
        states = self.session.query(State).all()
        state_new_updated_bills_dict = {}

        state_response_queue = Queue()
        state_new_updated_bills_dict = {}
        worker_thread = Thread(target=self._process, args=[state_response_queue, state_new_updated_bills_dict])
        worker_thread.start()
        
        for state in states:
            response = self.fetch(state)
            state_response_queue.put((state, response))
        state_response_queue.put((None, None))
        worker_thread.join()
        return state_new_updated_bills_dict
    
    def fetch(self, state):
        if self.use_cache:
            cached_response_filepath = Path("cache/{}.json".format(state.abbreviation))
            if cached_response_filepath.exists():
                with open(cached_response_filepath) as cached_response_file:
                    return json.load(cached_response_file)
        parameters = {"key": self.api_key, "op": "getMasterList", "state": state.abbreviation}
        response = requests.get(__class__.BASE_URL, parameters)
        assert response.status_code == 200
        if self.use_cache:
            with open(cached_response_filepath, 'w') as response_json_file:
                json.dump(response.json(), response_json_file)
        
        return response.json()
    
    def _process(self, state_response_queue, state_new_updated_bills_dict):
        while True:
            state, response = state_response_queue.get()
            if state is None:
                self.session.commit()
                return
            new_bills, updated_bills = get_new_updated_state_bills(self.session, state, response, self.keyphrase)
            if len(new_bills) > 0 or len(updated_bills) > 0:
                state_new_updated_bills_dict[state] = (new_bills, updated_bills)
            self.session.add_all(new_bills)
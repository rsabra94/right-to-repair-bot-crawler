from sqlalchemy import create_engine

from model.base import Base
from model.bill import Bill
from model.state import State

engine = create_engine("postgresql+psycopg2://postgres:postgres@localhost/right_to_repair", echo=True)
Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)
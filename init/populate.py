import re

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from model.bill import Bill
from model.state import State

REGEX_PATTERN = "(?P<name>([A-Za-z]+\s)*[A-Za-z]+)\s-\s(?P<abbreviation>[A-Z]{2})"

engine = create_engine("postgresql+psycopg2://postgres:postgres@localhost/right_to_repair", echo=False)
Session = sessionmaker(bind=engine)
session = Session()

# states.txt generated thanks to https://abbreviations.yourdictionary.com/articles/state-abbrev.html
with open("init/states.txt") as states_text_file:
    states_text = states_text_file.read()

for line in states_text.split("\n"):
    match_obj = re.match(REGEX_PATTERN, line)
    try:
        assert(match_obj)
    except AssertionError:
        raise Exception("Error matching \"{}\" against regex pattern.".format(line))
    name = match_obj.group("name")
    abbreviation = match_obj.group("abbreviation")
    state = State(name=name, abbreviation=abbreviation)
    session.add(state)

session.commit()
from model.bill import Bill

def get_new_updated_state_bills(session, state, response, keyphrase):
    existing_bills = session.query(Bill).filter_by(state=state).all()
    existing_bill_dict = {bill.bill_id: bill for bill in existing_bills}

    assert response["status"] == "OK"
    master_list = response["masterlist"]

    master_list_keys = list(master_list.keys())
    assert master_list_keys[0] == "session"

    master_list_keys = master_list_keys[1:]
    new_bills = []
    updated_bills = []
    for key in master_list_keys:
        bill_json_obj = master_list[key]
        bill_id = int(bill_json_obj["bill_id"])
        try:
            existing_bill = existing_bill_dict[bill_id]
            updated = existing_bill.update(bill_json_obj)
            if updated:
                updated_bills.append(existing_bill)
        except KeyError:
            found_keyphrase = False
            for text_category in ["last_action", "title", "description"]:
                text = bill_json_obj[text_category]
                if text is not None:
                    found_keyphrase = keyphrase in text.lower()
                    if found_keyphrase:
                        break
            if found_keyphrase:
                bill = Bill.from_json_obj(state, bill_json_obj)
                new_bills.append(bill)
    
    return new_bills, updated_bills